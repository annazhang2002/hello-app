require 'rails_helper'

RSpec.describe Micropost, type: :model do
    describe 'validations' do 
        it { is_expected.to validate_length_of(:content).is_at_most(140) }
        it { is_expected.to validate_presence_of(:content)}
    end
end